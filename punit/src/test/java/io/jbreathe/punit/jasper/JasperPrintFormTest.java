package io.jbreathe.punit.jasper;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JasperPrintFormTest {
    @Test
    void readFields() throws JRException {
        try (InputStream inputStream = JasperPrintFormTest.class.getResourceAsStream("/song_title.jrxml")) {
            Map<String, Object> params = new HashMap<>();
            params.put("PERFORMER", "New Order");
            params.put("ALBUM_NAME", "Music Complete");
            params.put("ALBUM_RELEASE_DATE", DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).format(LocalDate.of(2015, 9, 25)));
            params.put("SONG_NAME", "Tutti frutti");
            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
            JasperPrintForm printForm = new JasperPrintForm(jasperReport, jasperPrint);
            assertEquals("New Order", printForm.field("PERFORMER").orElseThrow(AssertionError::new).value());
            assertEquals("Music Complete", printForm.field("ALBUM_NAME").orElseThrow(AssertionError::new).value());
            assertEquals("September 25, 2015", printForm.field("ALBUM_RELEASE_DATE").orElseThrow(AssertionError::new).value());
            assertEquals("Tutti frutti", printForm.field("SONG_NAME").orElseThrow(AssertionError::new).value());
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
