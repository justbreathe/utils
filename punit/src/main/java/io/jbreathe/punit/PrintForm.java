package io.jbreathe.punit;

import java.util.Optional;

public interface PrintForm {
    Optional<PrintFormField> field(String id);
}
