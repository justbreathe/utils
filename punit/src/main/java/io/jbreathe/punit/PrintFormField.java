package io.jbreathe.punit;

public interface PrintFormField {
    String value();
}
