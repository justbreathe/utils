package io.jbreathe.punit.jasper;

import io.jbreathe.punit.PrintForm;
import io.jbreathe.punit.PrintFormField;
import net.sf.jasperreports.engine.JRPrintText;
import net.sf.jasperreports.engine.JRTextField;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public final class JasperPrintForm implements PrintForm {
    private final Map<String, JRPrintText> fields;

    public JasperPrintForm(JasperReport jasperReport, JasperPrint jasperPrint) {
        this.fields = parse(jasperReport, jasperPrint);
    }

    @Override
    public Optional<PrintFormField> field(String id) {
        return Optional.ofNullable(fields.get(id))
                .map(field -> new JasperPrintFormField(field.getFullText()));
    }

    // only "text fields" are supported
    private Map<String, JRPrintText> parse(JasperReport jasperReport, JasperPrint jasperPrint) {
        Map<UUID, String> uuidToFieldNames = Arrays.stream(jasperReport.getAllBands())
                .flatMap(band -> band.getChildren().stream())
                .filter(child -> child instanceof JRTextField textField && textField.getExpression().getChunks().length > 0)
                .map(child -> {
                    JRTextField textField = (JRTextField) child;
                    return Map.entry(textField.getUUID(), textField.getExpression().getChunks()[0].getText());
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return jasperPrint.getPages().stream()
                .flatMap(page -> page.getElements().stream())
                .filter(element -> element instanceof JRPrintText printText && uuidToFieldNames.get(printText.getUUID()) != null)
                .map(element -> {
                    JRPrintText printText = (JRPrintText) element;
                    return Map.entry(uuidToFieldNames.get(printText.getUUID()), printText);
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    record JasperPrintFormField(String value) implements PrintFormField {
    }
}
