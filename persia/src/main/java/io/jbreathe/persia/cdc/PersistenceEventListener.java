package io.jbreathe.persia.cdc;

public interface PersistenceEventListener {
    // TX
    void onTxBegin();

    void onTxCommit();

    void onTxRollback();

    // EM
    void onPersist(Object entity);

    void onMerge(Object before, Object after);

    void onRemove(Object entity);
}
