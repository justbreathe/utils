package io.jbreathe.persia.cdc;

import javax.annotation.concurrent.NotThreadSafe;
import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public final class EntityManagerFactoryProxy implements EntityManagerFactory {
    private final EntityManagerFactory delegate;
    private final List<Supplier<PersistenceEventListener>> listenerInitializers;

    private EntityManagerFactoryProxy(EntityManagerFactory delegate, List<Supplier<PersistenceEventListener>> listenerInitializers) {
        this.delegate = delegate;
        this.listenerInitializers = listenerInitializers;
    }

    public static Builder around(EntityManagerFactory delegate) {
        return new Builder(delegate);
    }

    @NotThreadSafe
    public static class Builder {
        private final EntityManagerFactory delegate;
        private final List<Supplier<PersistenceEventListener>> listenerInitializers = new ArrayList<>();

        private Builder(EntityManagerFactory delegate) {
            this.delegate = delegate;
        }

        public Builder addPersistentListenerInitializer(Supplier<PersistenceEventListener> listenerInitializer) {
            this.listenerInitializers.add(listenerInitializer);
            return this;
        }

        public EntityManagerFactoryProxy build() {
            return new EntityManagerFactoryProxy(delegate, Collections.unmodifiableList(listenerInitializers));
        }
    }

    @Override
    public EntityManager createEntityManager() {
        EntityManager entityManager = delegate.createEntityManager();
        return createEntityManagerProxy(entityManager);
    }

    @Override
    public EntityManager createEntityManager(Map map) {
        EntityManager entityManager = delegate.createEntityManager(map);
        return createEntityManagerProxy(entityManager);
    }

    @Override
    public EntityManager createEntityManager(SynchronizationType synchronizationType) {
        EntityManager entityManager = delegate.createEntityManager(synchronizationType);
        return createEntityManagerProxy(entityManager);
    }

    @Override
    public EntityManager createEntityManager(SynchronizationType synchronizationType, Map map) {
        EntityManager entityManager = delegate.createEntityManager(synchronizationType, map);
        return createEntityManagerProxy(entityManager);
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return delegate.getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return delegate.getMetamodel();
    }

    @Override
    public boolean isOpen() {
        return delegate.isOpen();
    }

    @Override
    public void close() {
        delegate.close();
    }

    @Override
    public Map<String, Object> getProperties() {
        return delegate.getProperties();
    }

    @Override
    public Cache getCache() {
        return delegate.getCache();
    }

    @Override
    public PersistenceUnitUtil getPersistenceUnitUtil() {
        return delegate.getPersistenceUnitUtil();
    }

    @Override
    public void addNamedQuery(String name, Query query) {
        delegate.addNamedQuery(name, query);
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        return delegate.unwrap(cls);
    }

    @Override
    public <T> void addNamedEntityGraph(String graphName, EntityGraph<T> entityGraph) {
        delegate.addNamedEntityGraph(graphName, entityGraph);
    }

    private EntityManager createEntityManagerProxy(EntityManager entityManager) {
        EntityManagerProxy.Builder builder = EntityManagerProxy.around(entityManager);
        listenerInitializers.stream()
                .map(Supplier::get)
                .forEach(builder::addPersistenceListener);
        return builder.build();
    }
}
