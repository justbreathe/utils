package io.jbreathe.persia.factory;

import javax.annotation.concurrent.NotThreadSafe;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class PersistenceUnit {
    public static Builder builder() {
        return new Builder();
    }

    @NotThreadSafe
    public static class Builder {
        private String persistenceUnitName;
        private String persistenceProviderClassName;
        private PersistenceUnitTransactionType transactionType;
        private DataSource jtaDataSource;
        private DataSource nonJtaDataSource;
        private final List<String> mappingFileNames = new ArrayList<>();
        private final List<URL> jarFileUrls = new ArrayList<>();
        private URL persistenceUnitRootUrl;
        private final List<String> managedClassNames = new ArrayList<>();
        private boolean excludeUnlistedClasses;
        private SharedCacheMode sharedCacheMode;
        private ValidationMode validationMode;
        private Properties properties;
        private String persistenceXMLSchemaVersion;
        private ClassLoader classLoader;
        private ClassLoader newTempClassLoader;

        private Builder() {
        }

        public Builder persistenceUnitName(String persistenceUnitName) {
            this.persistenceUnitName = persistenceUnitName;
            return this;
        }

        public Builder persistenceProviderClassName(String persistenceProviderClassName) {
            this.persistenceProviderClassName = persistenceProviderClassName;
            return this;
        }

        public Builder transactionType(PersistenceUnitTransactionType transactionType) {
            this.transactionType = transactionType;
            return this;
        }

        public Builder jtaDataSource(DataSource jtaDataSource) {
            this.jtaDataSource = jtaDataSource;
            return this;
        }

        public Builder nonJtaDataSource(DataSource nonJtaDataSource) {
            this.nonJtaDataSource = nonJtaDataSource;
            return this;
        }

        public Builder addMappingFileName(String mappingFileName) {
            this.mappingFileNames.add(mappingFileName);
            return this;
        }

        public Builder addJarFileUrl(URL jarFileUrl) {
            this.jarFileUrls.add(jarFileUrl);
            return this;
        }

        public Builder persistenceUnitRootUrl(URL persistenceUnitRootUrl) {
            this.persistenceUnitRootUrl = persistenceUnitRootUrl;
            return this;
        }

        public Builder addManagedClassName(String managedClassName) {
            this.managedClassNames.add(managedClassName);
            return this;
        }

        public Builder excludeUnlistedClasses(boolean excludeUnlistedClasses) {
            this.excludeUnlistedClasses = excludeUnlistedClasses;
            return this;
        }

        public Builder sharedCacheMode(SharedCacheMode sharedCacheMode) {
            this.sharedCacheMode = sharedCacheMode;
            return this;
        }

        public Builder validationMode(ValidationMode validationMode) {
            this.validationMode = validationMode;
            return this;
        }

        public Builder properties(Properties properties) {
            this.properties = properties;
            return this;
        }

        public Builder persistenceXMLSchemaVersion(String persistenceXMLSchemaVersion) {
            this.persistenceXMLSchemaVersion = persistenceXMLSchemaVersion;
            return this;
        }

        public Builder classLoader(ClassLoader classLoader) {
            this.classLoader = classLoader;
            return this;
        }

        public Builder newTempClassLoader(ClassLoader newTempClassLoader) {
            this.newTempClassLoader = newTempClassLoader;
            return this;
        }

        public PersistenceUnitInfo build() {
            return new PersistenceUnitInfo() {
                @Override
                public String getPersistenceUnitName() {
                    return persistenceUnitName;
                }

                @Override
                public String getPersistenceProviderClassName() {
                    return persistenceProviderClassName;
                }

                @Override
                public PersistenceUnitTransactionType getTransactionType() {
                    return transactionType;
                }

                @Override
                public DataSource getJtaDataSource() {
                    return jtaDataSource;
                }

                @Override
                public DataSource getNonJtaDataSource() {
                    return nonJtaDataSource;
                }

                @Override
                public List<String> getMappingFileNames() {
                    return mappingFileNames;
                }

                @Override
                public List<URL> getJarFileUrls() {
                    return jarFileUrls;
                }

                @Override
                public URL getPersistenceUnitRootUrl() {
                    return persistenceUnitRootUrl;
                }

                @Override
                public boolean excludeUnlistedClasses() {
                    return excludeUnlistedClasses;
                }

                @Override
                public List<String> getManagedClassNames() {
                    return managedClassNames;
                }

                @Override
                public SharedCacheMode getSharedCacheMode() {
                    return sharedCacheMode;
                }

                @Override
                public ValidationMode getValidationMode() {
                    return validationMode;
                }

                @Override
                public Properties getProperties() {
                    return properties;
                }

                @Override
                public String getPersistenceXMLSchemaVersion() {
                    return persistenceXMLSchemaVersion;
                }

                @Override
                public ClassLoader getClassLoader() {
                    return classLoader;
                }

                @Override
                public void addTransformer(ClassTransformer transformer) {
                    // no-op
                }

                @Override
                public ClassLoader getNewTempClassLoader() {
                    return newTempClassLoader;
                }
            };
        }
    }
}
