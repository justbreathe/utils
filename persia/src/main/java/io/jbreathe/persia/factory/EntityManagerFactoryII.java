package io.jbreathe.persia.factory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceProviderResolver;
import javax.persistence.spi.PersistenceProviderResolverHolder;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * Factory methods for {@link EntityManagerFactory}.
 */
public final class EntityManagerFactoryII {
    /**
     * Factory method to create an instance of a "non-JTA managed" {@link EntityManagerFactory}.
     * Can be used in managed environments with @PersistenceContext annotation.
     *
     * @param dataSource data source
     * @param properties persistence properties
     * @return an instance of a "non-JTA managed" EntityManagerFactory
     */
    public static EntityManagerFactory createManagedEntityManagerFactory(DataSource dataSource, Map<String, String> properties) {
        return createManagedEntityManagerFactory(dataSource, "entityManagerFactory", properties);
    }

    /**
     * Factory method to create an instance of a "non-JTA managed" {@link EntityManagerFactory}.
     * Can be used in managed environments with @PersistenceContext annotation.
     *
     * @param dataSource          data source
     * @param persistenceUnitName persistence unit name
     * @param properties          persistence properties
     * @return an instance of a "non-JTA managed" EntityManagerFactory
     */
    public static EntityManagerFactory createManagedEntityManagerFactory(DataSource dataSource, String persistenceUnitName, Map<String, String> properties) {
        PersistenceUnitInfo persistenceUnitInfo = PersistenceUnit.builder()
                .persistenceUnitName(persistenceUnitName)
                .nonJtaDataSource(dataSource)
                .build();
        PersistenceProviderResolver resolver = PersistenceProviderResolverHolder.getPersistenceProviderResolver();
        List<PersistenceProvider> providers = resolver.getPersistenceProviders();
        for (PersistenceProvider provider : providers) {
            EntityManagerFactory candidate = provider.createContainerEntityManagerFactory(persistenceUnitInfo, properties);
            if (candidate != null) {
                return candidate;
            }
        }
        throw new PersistenceException("No Persistence provider for EntityManager named '" + persistenceUnitName + "'.");
    }
}
